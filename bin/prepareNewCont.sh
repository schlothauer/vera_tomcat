#!/bin/bash

scriptPos=${0%/*}

if [ $# -eq 0 ]; then 
        echo -en "\033[1;34m the name of the container directory is as parameter needed \033[0m\n"
        echo "  $0 testHttpd"
        exit 1
fi
destDir=$scriptPos/../container/$1

if [ -d $destDir ]; then
        echo -en "\033[1;31m  destination directory already exists: $destDir \033[0m\n"
        exit 1
fi

echo -en "\033[1;34m  initialize directory: $destDir \033[0m\n"

mkdir -p $destDir/bin
cp $scriptPos/container/beispiel_conf.sh $destDir/bin/conf.sh
pushd $destDir/bin > /dev/null
ln -s ../../../bin/container/del_server.sh
ln -s ../../../bin/container/start_bash.sh
ln -s ../../../bin/container/start_server.sh
ln -s ../../../bin/container/stop_server.sh
popd > /dev/null

mkdir -p  $destDir/conf
echo <<README > $destDir/conf/README.txt
*.xml files are here located will be copied to /opt/tomcat/conf/Catalina/localhost
other file types will be ignored
README

mkdir -p  $destDir/webapps
echo <<README2 > $destDir/webapps/README.txt
Put your war files or extracted webapp directories to this folder
This directory is internally mounted to /opt/webapps. Configuration of context docBase should point 
to this directory.
*.conf files are here located will be linked to /etc/apache2/sites-enabled
other file types will be ignored
README2

mkdir -p  $destDir/docs
echo <<README3 > $destDir/docs/README.txt
Documents save by VERA+ are stored in this directory
README3
