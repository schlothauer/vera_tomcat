#!/bin/bash

scriptPos=${0%/*}


source "$scriptPos/conf.sh"
source "$scriptPos/../../../bin/image_conf.sh"

# is the name of linked database container set
if [ -z "$contVeraDb" ]; then
    echo -en "\033[1;34mVariable contVeraDb not set in conf.sh \033[0m\n"
    exit 1
fi

# try to start linked database container
if ! docker ps -f name="$contVeraDb" | grep "$contVeraDb" > /dev/null
then
    echo -en "\033[1;34mThe needed database container don't run: $contVeraDb \033[0m\n"
    if ! docker start $contVeraDb; then
        echo -en "\033[1;34mThe needed database container don't run: $contVeraDb \033[0m\n"    
        exit 1
    else
        echo -en "\033[1;34m  database container started: $contVeraDb \033[0m\n"
    fi
fi



# check for a already running container
docker ps -f name="$contNameServer" | grep "$contNameServer" > /dev/null && echo -en "\033[1;31m  Container läuft bereits: $contNameServer \033[0m\n" && exit 1

confDir=`pushd "$scriptPos/.." > /dev/null && pwd && popd > /dev/null`
webappsDir="$confDir/webapps"
docsDir="$confDir/docs"
confDir="$confDir/conf"

# check for a already existing image for the container
aktImgName=`docker images |  grep -G "$imageBase *$imageTag *" | awk '{print $1}'`
aktImgVers=`docker images |  grep -G "$imageBase *$imageTag *" | awk '{print $2}'`

if [ "$aktImgName" == "$imageBase" ] && [ "$aktImgVers" == "$imageTag" ]
then
        echo "run container from image: $aktImgName:$aktImgVers"
else
	if docker build -t $imageName $scriptPos/../../../image
    then
        echo -en "\033[1;34m  Image erstellt: $imageName \033[0m\n"
    else
        echo -en "\033[1;31m  Fehler beim Erstellen von Image: $imageName \033[0m\n"
        exit 1
    fi
fi

echo "$toHostPort"

# if a container already exists it's started
if docker ps -a -f name="$contNameServer" | grep "$contNameServer" > /dev/null; then
    docker start $contNameServer
else
    if [ -z "$toHostPort" ]; then
        docker run --name "$contNameServer" --cpuset-cpus=0-2 -d -v ${confDir}:/opt/vera/conf -v ${webappsDir}:/opt/vera/webapps -v ${docsDir}:/opt/vera/docs --link $contVeraDb:vera_db "$imageName"
    else
        echo "$toHostPort"
        docker run -it --rm --name "$contNameServer" --cpuset-cpus=0-2 -p $toHostPort:8080 -v ${confDir}:/opt/vera/conf -v ${webappsDir}:/opt/vera/webapps -v ${docsDir}:/opt/vera/docs --link $contVeraDb:vera_db "$imageName" /bin/bash
#        docker run --name "$contNameServer" --cpuset-cpus=0-2 -d -p $toHostPort:8080 -v ${confDir}:/opt/vera/conf -v ${webappsDir}:/opt/vera/webapps -v ${docsDir}:/opt/vera/docs --link $contVeraDb:vera_db "$imageName"
    fi
fi


