# Der Inhalt sollte für jeden Container angepasst werden
contVeraDb=vera_db_demo_server
# wenn toHostPort definiert ist, dann wird der Datenbankport auf den Hostport $toHostPort freigegeben
toHostPort=8080

contNameBase=vera_tomcat_demo
contNameServer=${contNameBase}_server
contNamePsql=${contNameBase}_psql

