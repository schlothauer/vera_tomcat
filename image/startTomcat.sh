#!/bin/bash

export LANG=de_DE.UTF-8
export LC_CTYPE="de_DE.UTF-8"
export LC_NUMERIC="de_DE.UTF-8"
export LC_TIME="de_DE.UTF-8"
export LC_COLLATE="de_DE.UTF-8"
export LC_MONETARY="de_DE.UTF-8"
export LC_MESSAGES="de_DE.UTF-8"
export LC_PAPER="de_DE.UTF-8"
export LC_NAME="de_DE.UTF-8"
export LC_ADDRESS="de_DE.UTF-8"
export LC_TELEPHONE="de_DE.UTF-8"
export LC_MEASUREMENT="de_DE.UTF-8"
export LC_IDENTIFICATION="de_DE.UTF-8"  

for f in /opt/vera/conf/*; do
    case "$f" in
    *.xml)    echo "$0: copying $f"; cp "$f" /opt/tomcat/conf/Catalina/localhost ;;
    *)        echo "$0: ignoring $f" ;;
    esac
    echo
done

export JAVA_OPTS="-Dfile.encoding=utf-8"

if [ "$1" = "debug" ] ; then
    JPDA_TRANSPORT="dt_socket"
    JPDA_ADDRESS="8000"
    JPDA_SUSPEND="n"
    JPDA_OPTS="-agentlib:jdwp=transport=$JPDA_TRANSPORT,address=$JPDA_ADDRESS,server=y,suspend=$JPDA_SUSPEND"
    export CATALINA_OPTS="$JPDA_OPTS $CATALINA_OPTS"
fi

/opt/tomcat/bin/catalina.sh run

